import { Component } from '@angular/core';

@Component({
  selector: 'app-basics',
  templateUrl: './basics.component.html',
  styles: [],
})
export class BasicsComponent {
  nameLower: string = 'jeanpier';
  nameUpper: string = 'JEANPIER';
  fullname: string = 'JEAnpIer MendOza';
  date: Date = new Date();
}
