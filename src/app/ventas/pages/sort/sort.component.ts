import { Component } from '@angular/core';
import { Color, Hero } from './../../entities/hero.entity';

@Component({
  selector: 'app-sort',
  templateUrl: './sort.component.html',
  styles: [],
})
export class SortComponent {
  isMayus: boolean = true;
  sortBy: string = '';

  heroes: Hero[] = [
    new Hero('Superman', true, Color.Blue),
    new Hero('Batman', false, Color.Black),
    new Hero('Robin', false, Color.Green),
    new Hero('Daredevil', false, Color.Red),
    new Hero('Green Latern', true, Color.Green),
  ];

  toggleMayus() {
    this.isMayus = !this.isMayus;
  }

  setSortBy(value: string) {
    if (this.sortBy === 'name') {
      this.sortBy = 'nameDesc';
      return;
    }
    this.sortBy = value;
  }
}
