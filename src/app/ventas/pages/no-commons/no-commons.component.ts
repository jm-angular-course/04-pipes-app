import { Component } from '@angular/core';
import { interval } from 'rxjs';
import { tap } from 'rxjs/operators';

type Gender = 'masculino' | 'femenino';
const initialName = 'Jeanpier';

@Component({
  selector: 'app-no-commons',
  templateUrl: './no-commons.component.html',
  styles: [],
})
export class NoCommonsComponent {
  // I18nselect Pipe
  // condicional de valor
  name: string = initialName;
  gender: Gender = 'masculino';
  invitationMap = {
    masculino: 'invitarlo',
    femenino: 'invitarla',
  };

  // I18nplural Pipe
  // condicional de cantidad
  customers: string[] = ['Maria', 'Pedro', 'Juan', 'José', 'Jesús'];
  customersMap = {
    '=0': 'no tenemos ningún cliente esperando',
    '=1': 'tenemos un cliente esperando',
    other: 'tenemos # clientes esperando',
  };

  changeCustomer() {
    if (this.name === initialName) {
      this.name = 'María';
      this.gender = 'femenino';
    } else {
      this.name = initialName;
      this.gender = 'masculino';
    }
  }

  deleteCustomer() {
    if (!this.customers.length) return;
    this.customers.pop();
  }

  // KeyValues Pipe
  person = {
    name: 'Jeanpier',
    age: 22,
    place: 'Guayaquil, Ecuador',
  };

  // Json Pipe
  heros = [
    { name: 'Superman', owner: 'DC', fly: true },
    { name: 'Batman', owner: 'DC', fly: false },
    { name: 'Thor', owner: 'Marvel', fly: false },
    { name: 'Spiderman', owner: 'Marvel', fly: false },
  ];

  // Async Pipe
  myObservable = interval(2000).pipe(tap(() => console.log('intervalo')));
  myPromise = new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve('Tenemos data de la promesa');
    }, 3500);
  });
}
