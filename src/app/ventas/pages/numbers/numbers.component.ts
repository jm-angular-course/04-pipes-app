import { Component } from '@angular/core';

@Component({
  selector: 'app-numbers',
  templateUrl: './numbers.component.html',
  styles: [],
})
export class NumbersComponent {
  ventasNetas: number = 2583743.398876;
  porcentaje: number = 0.473;
}
