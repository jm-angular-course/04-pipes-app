import { Pipe, PipeTransform } from '@angular/core';
import { Hero } from './../entities/hero.entity';

@Pipe({
  name: 'sort',
})
export class SortPipe implements PipeTransform {
  transform(value: Hero[], sortBy: string = ''): Hero[] {
    const sortNameAsc = (a: Hero, b: Hero) => (a.name > b.name ? 1 : -1);
    const sortNameDesc = (a: Hero, b: Hero) => (a.name > b.name ? -1 : 1);
    const sortFlyAsc = (a: Hero, b: Hero) => (a.fly > b.fly ? -1 : 1);
    const sortColorAsc = (a: Hero, b: Hero) => (a.color > b.color ? 1 : -1);

    switch (sortBy) {
      case 'name':
        return value.sort(sortNameAsc);
      case 'nameDesc':
        return value.sort(sortNameDesc);
      case 'fly':
        return value.sort(sortFlyAsc);
      case 'color':
        return value.sort(sortColorAsc);
      default:
        return value;
    }
  }
}
