export enum Color {
  Red,
  Black,
  Blue,
  Green,
}

export class Hero {
  constructor(public name: string, public fly: boolean, public color: Color) {}
}
