import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BasicsComponent } from './ventas/pages/basics/basics.component';
import { NoCommonsComponent } from './ventas/pages/no-commons/no-commons.component';
import { NumbersComponent } from './ventas/pages/numbers/numbers.component';
import { SortComponent } from './ventas/pages/sort/sort.component';

const routes: Routes = [
  { path: '', component: BasicsComponent, pathMatch: 'full' },
  { path: 'numbers', component: NumbersComponent },
  { path: 'no-commons', component: NoCommonsComponent },
  { path: 'sort', component: SortComponent },
  { path: '**', redirectTo: '' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRouterModule {}
